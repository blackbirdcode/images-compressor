# run this in any directory 
# add -v for verbose 
# get Pillow (fork of PIL) from
# pip before running -->
# pip install Pillow
  
# import required libraries
import os
import sys
import shutil

from pathlib import Path
from PIL import Image
  
# define a function for
# compressing an image
def compressMe(my_quality,dir,file,target, verbose = False):
    
    # Get the path of the file
    filepath = os.path.join(dir, 
                            file)
    print("the path of the file is :", filepath)  
    # open the image
    picture = Image.open(filepath)
      
    # Save the picture with desired quality
    # To change the quality of image,
    # set the quality variable at
    # your desired level, The more 
    # the value of quality variable 
    # and lesser the compression
    picture.save(target+"/"+file, 
                 "JPEG", 
                 optimize = True, 
                 quality = int(my_quality))
    return

def createDirByFileName(dir,file):
	table_name=file.split()
	numerocollection=table_name[0].split(":")
	name= numerocollection[0]+"_"+numerocollection[1]+"_"+table_name[1]
	dir_to_create = os.path.join(dir,
                            name)
	if not os.path.isdir(dir_to_create) :
		os.makedirs(dir_to_create)
	return dir_to_create

def convertMe(q,full_path,file,target_folder, verbose = False):
        filepath = os.path.join(full_path,file)
        im = Image.open(filepath)
        filename = Path(filepath).stem
        
        if im.mode in ("RGBA", "P"):
              im = im.convert("RGB")
        new_name=filename+'.jpg'
        new_pathfile=full_path+"/"+new_name
        
        im.save(new_pathfile)
        compressMe(q,full_path,new_name,target_folder, verbose)
        os.remove(new_pathfile)
        return

# Define a main function
def main():
    
    verbose = False
      
    # checks for verbose flag
    if (len(sys.argv)>1):
        
        if (sys.argv[1].lower()=="-v"):
            verbose = True
                      
    # finds current working dir
    cwd = os.getcwd()
    folder_name = input("Enter the folder name : ")
    q = input("Quality (1 is very poor 100 is very high) : ")
    full_path = "/".join([cwd, folder_name]) 
    target_name = "compressed_quality_"+q
    target_folder = cwd +"/" + target_name
    if os.path.isdir(target_folder) :
        shutil.rmtree(target_folder)
    os.makedirs(target_folder)
 
    formats = ('.jpg', '.jpeg')
    other_formats = ('.png','.psd')
      
    # looping through all the files
    # in a current directory

    for file in os.listdir(full_path):
        
        #target_folder_file = createDirByFileName(target_folder,file)
        
        # If the file format is JPG or JPEG
        if os.path.splitext(file)[1].lower() in formats:
            print('compressing', file)
            compressMe(q,full_path,file,target_folder, verbose)
        elif os.path.splitext(file)[1].lower() in other_formats:
            print("convert", file)
            convertMe(q,full_path,file,target_folder, verbose)
        else :
            os.remove(full_path+"/"+file)
	    
    print("Done")
  
# Driver code
if __name__ == "__main__":
    main()
