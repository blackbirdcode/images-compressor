# images-compressor

what is this doing ? :
- Compress one of your images folder
- In a given quality
- Convert your PNG to JPG at the same time


## Installation of the software
Please open a terminal

In order to be able to use the script you need to :
- Install python3
- Install Pillow (the library we will use to compress images and convert images)

you can do it by running the following script
IT IS WOKRING FOR MAC OS ONLY
``` bash
./install_macOS.sh
```
Then you should have Python3 install
check that by doing 
```bash
python -v
```
YOU SHOULD SEE Python 3.7 or something higher than 3


## Use the software

- Download the project
- Copy ONLY the script "compressImage.py" FROM the project ABOVE the folder of images you want to process 

Your folder should like the following :
- Your folder
- - compressImage.py
- - <your_images_folder>

Then go to your folder, open a terminal and do :
```bash 
cd <Path_to_your_folder>
```

Then you can run:
```bash 
python3 compressImage.py
```


The script will ask you which folder it should process
Then the Script will ask you the quality of image  you want to apply to your images

As ouput you will obtain:
- Your genuine folder image untouched
- A new folder of the processed images with only JPG or JPEG Compressed.
